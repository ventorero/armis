export const WEEKDAYS = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
export const DEFAULT_PERIOD = 24;
export const DEFAULT_HEATMAP = 10;
export const VALID_PERIODS = [...Array(24).fill(0).map((value,key) => key+1)]
                                .filter(n => (24 / n + 1) % 1 === 0);
export function formatTime(hours) {
    const suffix = (hours >= 12) ? 'PM' : 'AM';
    let time = (hours > 12) ? hours -12 : hours;
    time = time === 0 ? 12 : time;
    return time + suffix;
}

export function generateHueList(range) {
    let hueDivision = 270 / range;
    let hueList: number[] = [...Array(range)].map((val, key) => {
        return Math.floor(hueDivision * key);
    });
    return hueList.reverse();
}
