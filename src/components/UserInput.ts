import React, {useContext, useEffect, useState} from "react";
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import InputGroup from 'react-bootstrap/InputGroup';
import Button from 'react-bootstrap/Button';
import FormControl from 'react-bootstrap/FormControl';
import { DEFAULT_HEATMAP, VALID_PERIODS } from '../utils';
import { ArmisContext } from "../reducers/Reducer";

function PeriodInput() {
    // @ts-ignore
    const [ state, dispatch ] = useContext(ArmisContext);
    const selectHandler = selected => {
        dispatch({ type: 'UPDATE_PERIOD', payload: parseInt(selected) });
    }
    const periodList = VALID_PERIODS.map(p => {
        return React.createElement(Dropdown.Item,
            { key: p, eventKey: p, active: p === state.period, onSelect: selectHandler }, p);
    });

    return React.createElement(DropdownButton,
        { title: 'Period', size: 'sm', drop: 'down', className: 'period' },
        [...periodList]);
}

function HeatmapInput() {
    // @ts-ignore
    const [ ,dispatch ] = useContext(ArmisContext);
    const [ btnDisabled, setBtnDisabled ] = useState(true);
    const [ value, setValue ] = useState(DEFAULT_HEATMAP);

    const handleClick = () => {
        dispatch({ type: 'UPDATE_HUEMAP', payload: value });
    };
    const handleChange = e => {
        const _value = parseInt(e.target.value);
        setValue(_value);
        setBtnDisabled(_value < 1 || _value > 20);
    };

    // Init load
    useEffect(() => {
        dispatch({ type: 'UPDATE_HUEMAP', payload: value });
    }, []);

    return React.createElement(InputGroup, { size: 'sm', className: 'heatmap' },
            React.createElement(FormControl, { type: 'number', onChange: handleChange }, null),
            React.createElement(InputGroup.Append, { },
                React.createElement(Button, { disabled: btnDisabled, onClick: handleClick }, 'Heatmap Range')
            )
        );
}

export { PeriodInput, HeatmapInput };
