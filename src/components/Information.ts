import React from "react";
import Alert from 'react-bootstrap/Alert';
import hue from '../assets/hue.png';

function Information () {
    return React.createElement('div', { },
            React.createElement(Alert, { variant: 'secondary' },
                React.createElement(Alert.Heading, { }, 'Armis Heatmap Exercise\n'),
                React.createElement('p', { }, `
                    Heatmap Range can be between 1 to 20, which means the number of total shown colors. At default it is 10.
                    HSL is used to implement the color scale which the hue value can be between 0 to 360.
                    However, for better UX, implemented 0 to 270 
                `),
                React.createElement('img', { src: hue }, null)
            )
    );
}

export default Information;
