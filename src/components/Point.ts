import React, {useContext, useEffect, useState} from 'react';
import { ArmisContext } from "../reducers/Reducer";

/*
    Each point can be disabled, and in case of it is disabled
    the 'excludeData' function is called to rerender with the new data.
 */
function Point({ data, rangeMax, setExcludeData }) {
    // @ts-ignore
    const [ state ] = useContext(ArmisContext);
    const [ disabled, setDisabled ] = useState(false);
    const [ exclude, setExclude ] = useState({ counter: 0, list: [] });
    const hueKey = data.counter === rangeMax ? state.huemap.length - 1 :
                    Math.floor((state.huemap.length * data.counter) / rangeMax);
    const style = { backgroundColor: `hsl(${state.huemap[hueKey]}, 100%, 50%)` };

    const handleClick = () => {
        let tempExcludedList = {...exclude};
        const updatedExcludedList = !disabled ? {...data} : {};
        setExclude(updatedExcludedList);
        setDisabled(!disabled);

        // Add excluded list back to the main list
        setExcludeData(!disabled ? {...data}: tempExcludedList, !disabled);
    };

    useEffect(() => {
        setExclude({ counter: 0, list: [] });
        setDisabled(false);
    }, [state.period])

    return React.createElement('div', {
        className: disabled ? `point disabled` : `point`,
        onClick: handleClick,
        title: `Total Activities: ${data.counter}`,
        style
    }, null);
}

export default Point;
