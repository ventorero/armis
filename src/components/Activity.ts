import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';
import DayRow from './DayRow';
import { WEEKDAYS, formatTime } from '../utils';
import { ArmisContext } from '../reducers/Reducer';

function Activity({ list }) {
    // @ts-ignore
    const [ state ] = useContext(ArmisContext);
    const [ data, setData ] = useState(list);

    // Whenever the user clicks on dots to enable/disable, the state of data is updated
    const setExcludeData = (excludeList, disabled) => {
        const newData = disabled ? data.filter(ts => excludeList.list.indexOf(ts) < 0) :
                                    [...data, ...excludeList.list];
        setData(newData);
    };

    // Generates the matrix array with the first key of day of week
    const allList:Array<number[]> = [];
    WEEKDAYS.forEach(d => {
        allList[d] = Array(state.period).fill({}).map(() => { return { counter: 0, list: [] }; });
    });

    // Updates the allList with the data to pass it under DayRow component
    // In addition, it calculates the max value of the declared range
    let rangeMax = 0;
    const rangeSize:number = 24 / state.period;
    data.forEach(data => {
        const _date = new Date(data);
        const dayOfWeek = WEEKDAYS[_date.getDay()];
        const hourOfDay = _date.getHours();
        const range = Math.floor(hourOfDay / rangeSize);

        const countValue = allList[dayOfWeek][range]['counter'];
        const newCountValue = countValue + 1
        allList[dayOfWeek][range]['counter'] = newCountValue;
        allList[dayOfWeek][range]['list'] = [...allList[dayOfWeek][range]['list'], data];

        // Find the maximum
        rangeMax = rangeMax < newCountValue ? newCountValue : rangeMax;
    });

    // Time Ranges
    const timePeriod = [...Array(state.period)].map((value, key) => {
        const hours = formatTime(key * (24/state.period));
        return React.createElement('div', { key, className: 'time' }, hours);
    });

    // The last row that shows the time range
    const timeContainer =
        React.createElement('div', { className: 'row' },
            React.createElement('div', { className: 'header' }, 'Time'),
            [...timePeriod]
        );

    // Each DayRow by its with list of TSs
    const dayRows = Object.keys(allList).map(dayOfWeek => {
        return React.createElement(DayRow, {key: dayOfWeek, dayOfWeek, rangeMax, setExcludeData, list: allList[dayOfWeek]}, null);
    });

    return React.createElement('div', { },
            [...dayRows], timeContainer);
}

Activity.propTypes = {
    list: PropTypes.arrayOf(PropTypes.string)
};

export default Activity;
