import React from 'react';
import Point from './Point';

/*
    Each DayRow displaying the day of the week as header, and loops the points
 */
function DayRow({ list, dayOfWeek, rangeMax, setExcludeData }) {
    const points = list.map((data, key) => {
        return React.createElement(Point, { key, data, rangeMax, setExcludeData }, null);
    });

    return React.createElement('div', { className: 'row' },
            React.createElement('div', { className: 'header' }, dayOfWeek.substring(0,3)),
            [...points]);
}

export default DayRow;
