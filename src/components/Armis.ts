import React, { useReducer } from 'react';
import { PeriodInput, HeatmapInput } from "./UserInput";
import Activity from "./Activity";
import Information from './Information';
import { ArmisReducer, ArmisContext, initialState } from "../reducers/Reducer";

const list = require('../data.json'); // List of TSs

function Armis() {
    const [ state, dispatch ] = useReducer(ArmisReducer, initialState);
    // @ts-ignore
    return React.createElement(ArmisContext.Provider, { value: [state, dispatch] },
            React.createElement('div', { className: 'mainContainer' },
                React.createElement(Information, {}, null),
                React.createElement(PeriodInput, {}, null),
                React.createElement(HeatmapInput, {}, null),
                React.createElement(Activity, { list }, null)
            )
    );
}

export default Armis;
