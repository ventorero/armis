import React from 'react';
import ReactDOM from 'react-dom';
import Armis from './components/Armis';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

ReactDOM.render(
    React.createElement(Armis, { }, null),
    document.getElementById('root')
);

