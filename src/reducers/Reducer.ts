import { createContext } from "react";
import { DEFAULT_PERIOD, generateHueList } from "../utils";

export const initialState = {
    period: DEFAULT_PERIOD,
    huemap: []
}

export const ArmisReducer = (state, action) => {
    switch (action.type) {
        case 'UPDATE_PERIOD':
            return {
                ...state,
                period: action.payload
            };
        case 'UPDATE_HUEMAP':
            return {
                ...state,
                huemap: generateHueList(action.payload)
            };
        default:
            return state;
    }
};

export const ArmisContext = createContext(initialState);
