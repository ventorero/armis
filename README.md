# Armis Heatmap Exercise

Implemented React single page application to generate the list of activities from the hardcoded data.
Build by react-scripts ([create-react-app](https://reactjs.org/docs/create-a-new-react-app.html#create-react-app))

![](./readme_assets/activities2.png)

## Here is the structure of the app:

![](./readme_assets/main.png)

*  ```<Information>``` Component
    - Static component just shows the information panel


*  ```<PeriodInput>``` Component
    - The dropdown that gives an option to select the range of period in hours. It can be 1,2,3,4,6,8,12,24 hours of period.
      Whenever the user does the selection it updates the state. Sample of 8 periods in a day;
      ![](./readme_assets/sample8.png)

      
*  ```<HeadmapInput>``` Component
    - Input field for the heatmap implementation. The valid input is 1 to 20, otherwise the button is disabled. At default it is 10.
    The value is the number of colors shown in Activity chart. The bigger number shows the more info on activities. Sample of just 2 colors;
      ![](./readme_assets/sample2heatmap.png)



*  ```<Activity>``` Component
    - The main logic is implemented here. It gets the list and the period as parameters.
       According to these parameters it generates loop for each day (DayRow).
       At the end it generates the row to show the time range. 

        ![](./readme_assets/comps.png)

* ```<DayRow>``` Component
    - It generates the row with the header (day of the week).
      It loops the Point component with just the data of the relevant day and time range.

* ```<Point>``` Component
    - Shows each dot with calculation of its color.
      As mentioned above, it gets from its parent the relevant data and the maximum value of the activities according to the declared period range
      
    ![](./readme_assets/point_data.png)


## List of open source Libraries / APIs used

* [React (17.0.2)](https://reactjs.org/)
* [React DOM](https://reactjs.org/)
* [react-scripts (4.0.3)](https://https://create-react-app.dev/) - Used by create-react-app.
    * I used it, because I wanted an easy start but it is not the best way for real life implementations.
* [prop-types](https://www.npmjs.com/package/prop-types) - I used a custom prop type for the ``<Activity />`` props
* [react-bootstrap](https://react-bootstrap.github.io/) - Used for styling


## Development server

There are no changes on default create-react-app configuration. Just run `npm install` for the first time.
To run application `npm start` and navigate to `http://localhost:3000/`.

